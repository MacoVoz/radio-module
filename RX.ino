#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

int led = 13;

#define PWMA 23
#define PWMB 17
#define AIN1 21
#define AIN2 22
#define BIN1 19
#define BIN2 18
#define STBY 20

#define f_speed 100
#define min_speed 50
#define max_speed 150
#define DEL 50

void forward();

void right();
void left();
void readd();
void motor();


RF24 radio(9,10); 

byte address[][6] = {"1Node","2Node","3Node","4Node","5Node","6Node"};

void setup()
{
  pinMode(PWMA, OUTPUT);  
  pinMode(PWMB, OUTPUT);  
  pinMode(AIN1, OUTPUT);  
  pinMode(AIN2, OUTPUT);  
  pinMode(BIN1, OUTPUT);  
  pinMode(BIN2, OUTPUT);  
  pinMode(STBY, OUTPUT);
  pinMode(0, INPUT);
  pinMode(1, INPUT);
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(4, INPUT);
  pinMode(5, INPUT);

  pinMode(led, OUTPUT); 
  Serial.begin(9600); //открываем порт для связи с ПК
  radio.begin(); //активировать модуль
  radio.setAutoAck(1);         
  radio.setRetries(0,15);     
  radio.enableAckPayload();    
  radio.setPayloadSize(32);     

  radio.openReadingPipe(1,address[0]); 
  radio.setChannel(0x60); 

  radio.setPALevel (RF24_PA_MAX);
  radio.setDataRate (RF24_250KBPS); 
  
  radio.powerUp(); 
  radio.startListening(); 
}

int num = 0;
byte gotByte; 

void loop() 
{ 
    byte pipeNo;                        
    while( radio.available(&pipeNo))
    { 
      radio.read( &gotByte, sizeof(gotByte) );       

      Serial.print("Recieved: "); Serial.println(gotByte);
         if(gotByte == 3)
   {
    forward();
   }
      if(gotByte == 1)
   {
    left();
   }
      if(gotByte == 2)
   {
    right();
   }
   }
}


void forward()
{
  digitalWrite(STBY, HIGH); 
  digitalWrite(AIN1, LOW); 
  digitalWrite(AIN2, HIGH); 
  analogWrite(PWMA, f_speed);
  digitalWrite(BIN1, HIGH); 
  digitalWrite(BIN2, LOW); 
  analogWrite(PWMB, f_speed);
}

void right()
{
  digitalWrite(STBY, HIGH); 
  digitalWrite(AIN1, LOW); 
  digitalWrite(AIN2, HIGH); 
  analogWrite(PWMA, max_speed);
  digitalWrite(BIN1, HIGH); 
  digitalWrite(BIN2, LOW); 
  analogWrite(PWMB, min_speed);
}

void left()
{
  digitalWrite(STBY, HIGH); 
  digitalWrite(AIN1, LOW); 
  digitalWrite(AIN2, HIGH); 
  analogWrite(PWMA, min_speed);
  digitalWrite(BIN1, HIGH); 
  digitalWrite(BIN2, LOW); 
  analogWrite(PWMB, max_speed);
}
